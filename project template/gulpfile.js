var gulp = require('gulp');
elixir = require('laravel-elixir');

elixir(function (mix) {
    mix.sass('app.scss', 'public/css/');
    mix.version([
        '/css/app.css',
    ]);
});
